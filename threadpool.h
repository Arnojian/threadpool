#ifndef _THREADPOOL_H_
#define _THREADPOOL_H_

#include <list>
#include <cstdio>
#include <exception>
#include <pthread.h>

#include "locker.h"

template <typename T>
class threadpool
{
	public:
		threadpool(int threadNumber=8,int maxRequests=100);
		~threadpool();
		bool append(T* request);
	private:
		static void* worker(void* arg);
		void run() ;
	
	private:
		int m_threadNumber ;// 线程数
		int m_maxRequests;     // 最大的请求数
		pthread_t *m_threads ; //子线程数组
		std::list<T *> m_workQueue ;// 请求队列
		locker m_queueLocker ; // 请求队列锁
		sem m_queueStat ; // 是否需要处理，子线程都睡在这里
		bool m_stop ;     //销毁
};

template <typename T>
threadpool<T>::threadpool(int threadNumber,int maxRequests)
{
	if(threadNumber <=0 || maxRequests <=0)
	{
		throw std::exception();
	}

	m_threads = new pthread_t[ m_threadNumber] ;
	if(!m_threads)
	{
		throw std::exception() ;
	}

	for( int i = 0 ;i< threadNumber; i++)
	{
		printf("create the %dth thread\n",i) ;
		if(ptread_create(m_threads+i,NULL,worker,this)!=0)
		{
			delete [] m_threads;
			throw std::exception() ;
		}

		if(pthread_detach(m_threads[i]))
		{
			delete [] m_threads ;
			throw std::exception() ;
		}
	}
}

template<typename T>
threadpool<T>::~threadpool()
{
	delete [] m_threads ;
	m_stop = true ;
}

template<typename T>
bool threadpool<T>::append(T *request)
{
	m_queueLocker.lock() ;
	
	if(m_workQueue.size() > m_maxRequests)
	{
		m_queueLocker.unlock() ;
		printf("request queue is full\n") ;
		return false ;
	}
	
	m_workQueue.push_back(request) ;
	m_queueLocker.unlock() ;
	
	m_queueStat.post() ;
	return true ;
}

template<typename T>
void * threadpool<T>::worker(void *arg)
{
	threadpool* pool = (threadpool*)arg;
	pool->run() ;
	return pool ;
}

template<typename T>
void threadpool<T>::run()
{
	while(!m_stop)
	{
		m_queueStat.wait() ;
		m_queueLocker.lock() ;
		
		if(m_workQueue.empty())
		{
			m_queueLocker.unlock() ;
			continue ;
		}
		
		T* request = m_workQueue.front();
		m_workQueue.pop_front() ;
		m_queueLocker.unlock() ;

		if(!request)
		{
			continue ;
		}
			
		request->process() ;// 请求的处理函数

	}
}



#endif
