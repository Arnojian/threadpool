#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <assert.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>

const char * request = "luojian hahha";

int setNonBlocking(int fd)
{
	int oldOption = fcntl(fd,F_GETFL);
	int newOption = oldOption & O_NONBLOCK ;
	fcntl(fd,F_SETFL,newOption);
	return oldOption;
}

void addFd(int epollfd,int fd)
{
	epoll_event event ;
	event.data.fd = fd ;
	event.events = EPOLLIN | EPOLLERR | EPOLLET;
	epoll_ctl(epollfd,EPOLL_CTL_ADD,fd,&event);
	setNonBlocking(fd);
}

void removeFd(int epollfd,int fd)
{
	epoll_ctl(epollfd,EPOLL_CTL_DEL,fd,0);
}

bool write_nbytes(int sockfd,const char *buffer,int len)
{
	int bytesWrite = 0 ;
	printf("write out %d bytes to socket %d\n",len,sockfd);
	while(1)
	{
		bytesWrite = send(sockfd,buffer,len,0);
		if(bytesWrite == -1)
		{
			if(errno == EAGAIN || errno == EWOULDBLOCK || errno==EINTR)
			{
				continue ;
			}
			else
			{
				return false ;
			}
		}
		else if(bytesWrite==0)
		{
			return false ;
		}
		else
		{
			len -=bytesWrite ;
			buffer = buffer+bytesWrite;
			if(len <=0)
			{
				return true ;
			}
		}
	}
}

bool read_once(int sockfd,char *buffer,int len)
{
	int bytesRead = 0 ;
	bytesRead = recv(sockfd,buffer,len,0);
	if(bytesRead==-1)
	{
		return false ;
	}
	else if(bytesRead==0)
	{
		return false ;
	}
	buffer[bytesRead]='\0' ;
	printf("read in %d bytes from socket %d with content:%s\n",bytesRead,sockfd,buffer);
	return true ;
}

void start_conn(int epollfd,int num,const char *ip,int port)
{
	struct sockaddr_in address;
	memset(&address,0x00,sizeof(address));
	
	address.sin_family= AF_INET;
	inet_pton(AF_INET,ip,&address.sin_addr);
	address.sin_port = htons(port);
	
	for(int i = 0 ;i<num;i++)
	{
		sleep(1);
		int sockfd = socket(PF_INET,SOCK_STREAM,0);
		assert(sockfd >=0);

		if( connect(sockfd,(struct sockaddr*)&address,sizeof(address))==0)
		{
			printf("connect\n");
			addFd(epollfd,sockfd);
		}
	}
}

void close_conn(int epollfd,int sockfd)
{
	removeFd(epollfd,sockfd);
	close(sockfd);
}

int main(int argc,char* argv[])
{
	assert(argc==4);
	int epollfd = epoll_create(100);
	start_conn(epollfd,atoi(argv[3]),argv[1],atoi(argv[2]));
	epoll_event events[10000];
	char buffer[2048];
	struct epoll_event event ;
	while(1)
	{
		int fds = epoll_wait(epollfd,events,10000,2000);
		for(int i = 0 ;i<fds;i++)
		{
			int sockfd = events[i].data.fd ;
			if(events[i].events & EPOLLIN)
			{
				if(!read_once(sockfd,buffer,2048))
				{
					close_conn(epollfd,sockfd);
					continue ;
				}
				memset(&event,0x00,sizeof(event));
				event.data.fd = sockfd;
				event.events = EPOLLOUT | EPOLLERR | EPOLLET;
				epoll_ctl(epollfd,EPOLL_CTL_MOD,sockfd,&event);
			}
			else if(events[i].events & EPOLLOUT)
			{
				if(!write_nbytes(sockfd,request,strlen(request)))
				{
					close_conn(epollfd,sockfd);
					continue;
				}
				memset(&event,0x00,sizeof(event));
				event.data.fd = sockfd;
				event.events = EPOLLIN|EPOLLERR|EPOLLET;
				epoll_ctl(epollfd,EPOLL_CTL_MOD,sockfd,&event);
			}
			else if(events[i].events & EPOLLERR)
			{
				close_conn(epollfd,sockfd);
			}
		}
	}
}

